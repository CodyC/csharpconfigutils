﻿using System.Configuration;

namespace ConfigUtilsLib
{
    public class ConfigUtils
    {
        public static void UpdateAppSettingConfigValue(string settingKeyLiteral, string settingValueToSet)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.AppSettings.Settings[settingKeyLiteral].Value = settingValueToSet;
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }

    }
}
